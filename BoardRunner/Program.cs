﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace BoardRunner
{
    class Program
    {
        static void Main(string[] args)
        {
            int rowCount = int.Parse(args[0]);
            int columnCount = int.Parse(args[1]);

            List<Process> processList = new List<Process>();

            for (int r = 0; r < rowCount; r++)
            {
                for (int c = 0; c < columnCount; c++)
                {
                    var cellProcess = StartCellProcess(r, c, rowCount, columnCount);
                    processList.Add(cellProcess);
                }
            }

            AppDomain.CurrentDomain.ProcessExit += (sender, eventArgs) => processList.ForEach(p => p.Kill());

            Console.WriteLine("Hit ESC to exit...");
            ConsoleKeyInfo consoleKeyInfo;
            do
            {
                consoleKeyInfo = Console.ReadKey();
            } while (consoleKeyInfo.Key != ConsoleKey.Escape);
        }

        private static Process StartCellProcess(int row, int column, int rowCount, int columnCount)
        {
            var neighbors = string.Join(", ", GetNeighbors(row, column, rowCount, columnCount));
            var name = $"{row:D2}{column:D2}";
            Console.WriteLine($"Starting [{name}]...");
            var arguments =
                $"CellService.dll /ServiceUrl=\"localhost:8000\" /Name={name} /Neighbors=\"{neighbors}\"";
            var processStartInfo = new ProcessStartInfo()
            {
                FileName = "dotnet",
                Arguments = arguments,
                WorkingDirectory = @"..\..\..\..\CellService\bin\Debug\netcoreapp2.2\"
            };
            var process = Process.Start(processStartInfo);
            return process;
        }

        private static IEnumerable<string> GetNeighbors(int row, int column, int rowCount, int columnCount)
        {
            yield return $"{Above(row, rowCount):D2}{Left(column, columnCount):D2}";
            yield return $"{Above(row, rowCount):D2}{column:D2}";
            yield return $"{Above(row, rowCount):D2}{Right(column, columnCount):D2}";
            yield return $"{row:D2}{Left(column, columnCount):D2}";
            yield return $"{row:D2}{Right(column, columnCount):D2}";
            yield return $"{Below(row, rowCount):D2}{Left(column, columnCount):D2}";
            yield return $"{Below(row, rowCount):D2}{column:D2}";
            yield return $"{Below(row, rowCount):D2}{Right(column, columnCount):D2}";
        }

        private static int Right(int column, int columnCount) => column + 1 == columnCount ? 0 : column + 1;

        private static int Below(int row, int rowCount) => row + 1 == rowCount ? 0 : row + 1;

        private static int Left(int column, int columnCount) => column - 1 >= 0 ? column - 1 : columnCount - 1;

        private static int Above(int row, int rowCount) => row - 1 >= 0 ? row - 1 : rowCount - 1;
    }
}