﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Messages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace BoardService
{
    internal class BoardService : Board.Board.BoardBase, IHostedService
    {
        private readonly ConcurrentDictionary<string, ConcurrentDictionary<string, BlockingCollection<BoardEvent>>> _cellStateSubscriptions;
        private readonly ConcurrentDictionary<string, BlockingCollection<BoardEvent>> _generalSubscriptions;
        private readonly SemaphoreSlim _subCountSync;
        private readonly int _expectedSubscriberCount;
        private int _currentSubscriberCount;
        private Server _server;

        public BoardService(IConfiguration configuration)
        {
            _expectedSubscriberCount = configuration.GetSection("SubscriberCount").Get<int>();
            _cellStateSubscriptions = new ConcurrentDictionary<string, ConcurrentDictionary<string, BlockingCollection<BoardEvent>>>();
            _generalSubscriptions = new ConcurrentDictionary<string, BlockingCollection<BoardEvent>>();
            _subCountSync = new SemaphoreSlim(1, 1);
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _server = new Server
            {
                Services = { Board.Board.BindService(this) },
                Ports = { new ServerPort("localhost", 8000, ServerCredentials.Insecure) }
            };

            _server.Start();
            return Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            await _server.ShutdownAsync();
            _subCountSync.Dispose();
        }

        public override Task<Empty> Poke(Tick request, ServerCallContext context)
        {
            var boardEvent = new BoardEvent()
            {
                TimerTick = request
            };

            QueueEvent(boardEvent, _generalSubscriptions.Values);

            return Task.FromResult(new Empty());
        }

        public override Task<Empty> ReportState(CellState request, ServerCallContext context)
        {
            if (_cellStateSubscriptions.TryGetValue(request.Name, out var subscribers))
            {
                var boardEvent = new BoardEvent()
                {
                    NeighborStateUpdate = request
                };
                QueueEvent(boardEvent, subscribers.Values);
            }

            if (_cellStateSubscriptions.TryGetValue("*", out var wildcards))
            {
                var boardEvent = new BoardEvent()
                {
                    NeighborStateUpdate = request
                };
                QueueEvent(boardEvent, wildcards.Values);
            }

            return Task.FromResult(new Empty());
        }

        public override async Task Subscribe(Subscription request, IServerStreamWriter<BoardEvent> responseStream, ServerCallContext context)
        {
            var messageQueue = new BlockingCollection<BoardEvent>();

            foreach (var topic in request.Topic)
            {
                var topicSubscriptions = _cellStateSubscriptions.GetOrAdd(topic, _ => new ConcurrentDictionary<string, BlockingCollection<BoardEvent>>());
                topicSubscriptions.TryAdd(request.Subscriber, messageQueue);
            }

            _generalSubscriptions.TryAdd(request.Subscriber, messageQueue);

            await _subCountSync.WaitAsync(context.CancellationToken);

            _currentSubscriberCount += 1;

            if (_currentSubscriberCount >= _expectedSubscriberCount)
            {
                var boardEvent = new BoardEvent()
                {
                    BoardReady = new BoardReady()
                };
                QueueEvent(boardEvent, _generalSubscriptions.Values);
            }

            _subCountSync.Release();

            foreach (var boardEvent in messageQueue.GetConsumingEnumerable(context.CancellationToken))
            {
                await responseStream.WriteAsync(boardEvent);
            }
        }

        private void QueueEvent(BoardEvent boardEvent, ICollection<BlockingCollection<BoardEvent>> subs)
        {
            foreach (var boardEventSub in subs)
            {
                boardEventSub.Add(boardEvent);
            }
        }
    }
}