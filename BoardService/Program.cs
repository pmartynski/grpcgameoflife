﻿using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace BoardService
{
    class Program
    {
        static async Task Main(string[] args)
        {

            var host = new HostBuilder()
                .ConfigureAppConfiguration((ctx, configuration) =>
                {
                    configuration.AddCommandLine(args);
                })
                .ConfigureLogging((ctx, logging) =>
                {
                    logging.AddConsole();
                })
                .ConfigureServices((ctx, services) =>
                {
                    services.AddHostedService<BoardService>();
                })
                .Build();

            await host.RunAsync();
        }
    }
}
