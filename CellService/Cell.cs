﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Messages;

namespace CellService
{
    internal sealed class Cell : IDisposable
    {
        private readonly Dictionary<string, (int, CellLife)> _neighbors;
        private readonly CancellationTokenSource _cts;
        private CellLife _life;
        private int _tick;

        public BlockingCollection<object> CommandQueue { get; }

        public CellLife Life => _life;
        public int Tick => _tick;

        public string Name { get; }

        public event Action<CellState> StateChangedEvent;

        public Cell(string name, CellLife? initial = null)
        {
            _life = initial ?? Random();
            Name = name;
            _tick = 0;
            _neighbors = new Dictionary<string, (int tick, CellLife state)>();

            CommandQueue = new BlockingCollection<object>();
            _cts = new CancellationTokenSource();
            Run(_cts.Token);
        }

        private void Run(CancellationToken token)
        {
            Task.Run(() =>
            {
                foreach (var cmd in CommandQueue.GetConsumingEnumerable(token))
                {
                    ExecuteCommand(cmd);
                }
            }, token);
        }

        private CellLife Random() => (CellLife)new Random().Next(1, 3);

        private void ExecuteCommand(object cmd)
        {
            switch (cmd)
            {
                case CellState nsCmd:
                    if (!_neighbors.TryGetValue(nsCmd.Name, out (int tick, CellLife state) oldState)
                        || oldState.tick < nsCmd.Tick.Cycle)
                    {
                        _neighbors[nsCmd.Name] = (nsCmd.Tick.Cycle, nsCmd.State);
                    }

                    break;
                case Tick tickCmd:
                    _tick = Math.Max(_tick, tickCmd.Cycle);
                    var oldVal = _life;
                    int neighborsAlive = _neighbors.Values.Count(v => v.Item2 == CellLife.Alive);

                    if (oldVal == CellLife.Dead && neighborsAlive == 3)
                    {
                        _life = CellLife.Alive;
                    }
                    else if (oldVal == CellLife.Alive && (neighborsAlive < 2 || neighborsAlive > 3))
                    {
                        _life = CellLife.Dead;
                    }

                    if (oldVal != _life)
                    {
                        StateChangedEvent?.Invoke(new CellState()
                        {
                            Name = Name,
                            State = _life,
                            Tick = new Tick { Cycle = _tick }
                        });
                    }

                    break;

                default:
                    throw new ArgumentException("Unknown command", nameof(cmd));
            }
        }

        public void Dispose()
        {
            _cts.Cancel();
            _cts?.Dispose();
            CommandQueue?.Dispose();
        }
    }
}