﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Messages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace CellService
{
    class CellService : IHostedService
    {
        private AsyncServerStreamingCall<BoardEvent> _boardEventStream;
        private Board.Board.BoardClient _boardClient;
        private Channel _channel;
        private readonly string _name;
        private readonly string[] _neighbors;
        private readonly string _serviceUrl;
        private Cell _cell;

        public CellService(IConfiguration configuration)
        {
            _name = configuration.GetSection("Name").Get<string>();
            _neighbors = configuration.GetSection("Neighbors").Get<string>().Split(",", StringSplitOptions.RemoveEmptyEntries).Select(s => s.Trim()).ToArray();
            _serviceUrl = configuration.GetSection("ServiceUrl").Get<string>();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _channel = new Channel(_serviceUrl, ChannelCredentials.Insecure);
            _boardClient = new Board.Board.BoardClient(_channel);
            var subscription = new Subscription() { Subscriber = _name };
            subscription.Topic.AddRange(_neighbors);
            _boardEventStream = _boardClient.Subscribe(subscription, cancellationToken: cancellationToken);
            Consume(_boardEventStream, cancellationToken);
            _cell = new Cell(_name);
            _cell.StateChangedEvent += CellOnStateChangedEvent;
            return Task.CompletedTask;
        }

        private async void CellOnStateChangedEvent(CellState obj)
        {
            await _boardClient.ReportStateAsync(obj);
        }

        private void Consume(AsyncServerStreamingCall<BoardEvent> boardEventStream, CancellationToken cancellationToken)
        {
            var stream = boardEventStream.ResponseStream;
            Task.Run(async () =>
            {
                while (await stream.MoveNext(cancellationToken))
                {
                    switch (stream.Current.CommandCase)
                    {
                        case BoardEvent.CommandOneofCase.BoardReady:
                            Console.WriteLine(stream.Current.CommandCase.ToString());
                            await _boardClient.ReportStateAsync(new CellState
                                {Name = _cell.Name, State = _cell.Life, Tick = new Tick {Cycle = _cell.Tick}});
                            break;

                        case BoardEvent.CommandOneofCase.TimerTick:
                            Console.WriteLine(stream.Current.CommandCase.ToString());
                            _cell.CommandQueue.Add(stream.Current.TimerTick);
                            break;

                        case BoardEvent.CommandOneofCase.NeighborStateUpdate:
                            Console.WriteLine(stream.Current.CommandCase.ToString());
                            _cell.CommandQueue.Add(stream.Current.NeighborStateUpdate);
                            break;
                    }
                }
            });
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _boardEventStream.Dispose();
            return Task.CompletedTask;
        }
    }
}