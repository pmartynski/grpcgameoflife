﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Messages;

namespace Client
{
    class Program
    {
        public static Board.Board.BoardClient BoardClient { get; set; }
        public static AsyncServerStreamingCall<BoardEvent> CellsSubscription { get; set; }
        public static Task CellConsumerTask { get; set; }
        public static CancellationTokenSource CellSubCancellation { get; set; }

        static async Task Main(string[] args)
        {
            await Repl();
        }

        private static async Task Repl()
        {
            while (true)
            {
                Console.Write("> ");
                var cmd = Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries);

                try
                {
                    switch (cmd.FirstOrDefault())
                    {
                        case "connect":
                            Connect(cmd.Skip(1).First());
                            break;

                        case "tick":
                            await Tick(Int32.Parse(cmd.Skip(1).First()));
                            break;

                        case "subscribe":
                            Subscribe(cmd.Skip(1));
                            break;

                        case "help":
                            Console.WriteLine("TODO");
                            break;

                        case "exit":
                            Environment.Exit(0);
                            break;

                        default:
                            Console.WriteLine("WAT!?");
                            break;
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine($"Syntax error {ex.Message}");
                }
            }
        }

        private static void Connect(string host)
        {
            var channel = new Channel(host, ChannelCredentials.Insecure);
            BoardClient = new Board.Board.BoardClient(channel);
        }

        private static void Subscribe(IEnumerable<string> topics)
        {
            if (BoardClient == null)
            {
                throw new Exception("Client not connected");
            }

            var subscription = new Subscription()
            {
                Subscriber = "Console"
            };
            subscription.Topic.AddRange(topics);
            CellsSubscription = BoardClient.Subscribe(subscription);
            CellSubCancellation = new CancellationTokenSource();
            CellConsumerTask = Task.Run(() => Consume(CellSubCancellation.Token), CellSubCancellation.Token);
        }

        private static async Task Consume(CancellationToken token)
        {
            while (await CellsSubscription.ResponseStream.MoveNext(token))
            {
                var boardEvent = CellsSubscription.ResponseStream.Current;

                Console.SetCursorPosition(0, Console.CursorTop);

                Console.WriteLine();
                Console.WriteLine("Message received:");
                Console.WriteLine(boardEvent.CommandCase.ToString());
                switch (boardEvent.CommandCase)
                {
                    case BoardEvent.CommandOneofCase.NeighborStateUpdate:
                        Console.WriteLine($"Name: {boardEvent.NeighborStateUpdate.Name}");
                        Console.WriteLine($"State: {boardEvent.NeighborStateUpdate.State}");
                        Console.WriteLine($"Tick.Cycle: {boardEvent.NeighborStateUpdate.Tick.Cycle}");
                        break;
                    case BoardEvent.CommandOneofCase.BoardReady:
                        break;
                    case BoardEvent.CommandOneofCase.TimerTick:
                        Console.WriteLine($"Cycle: {boardEvent.TimerTick.Cycle}");
                        break;

                    default:
                        Console.WriteLine("Unknown");
                        break;
                }

                Console.WriteLine();

                Console.Write("> ");
            }
        }

        private static async Task Tick(int cycle)
        {
            await BoardClient.PokeAsync(new Tick { Cycle = cycle });
        }
    }
}
